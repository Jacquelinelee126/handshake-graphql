import React from 'react';
import { Route, Switch, Redirect } from 'react-router';
import UserPage from '../../pages/user';
import { BrowserRouter } from 'react-router-dom';
import HomePage from '../../pages/home';

import StudentPage from "../../pages/student";
import CompanyPage from "../../pages/company";
import SearchJobPage from "../../pages/search/job";
import SearchStudentPage from "../../pages/search/student";

export default function SiteRoute() {
    return (
        <BrowserRouter>
            <Switch>
                <Route path="/home">
                    <HomePage />
                </Route>

                <Route path="/user">
                    <UserPage />
                </Route>

                <Route path="/student">
                    <StudentPage />
                </Route>
                <Route path="/company">
                    <CompanyPage />
                </Route>

                <Route exact path="/search/job">
                    <SearchJobPage />
                </Route>
               
                <Route exact path="/search/student">
                    <SearchStudentPage />
                </Route>
                <Redirect to="/home" />
            </Switch>
        </BrowserRouter>
    );
}
