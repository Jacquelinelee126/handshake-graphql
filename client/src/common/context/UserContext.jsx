import React, { useContext, createContext, useEffect } from 'react';
import useUser from '../../models/useUser';
import { useQuery } from '@apollo/react-hooks';
import { INIT } from '../../pages/user/queries';
import { useHistory } from 'react-router-dom';

const UserContext = createContext(null);
export const useUserContext = () => useContext(UserContext);

export default ({ children }) => {
    // let history = useHistory();
    const { user, initUser } = useUser();

    console.log('user in context 1', user);

    const { data, loading, error } = useQuery(INIT);

    console.log('data context', data);

    useEffect(() => {
        data && initUser(data.currentUser);
    }, [data]);

    return <UserContext.Provider value={{ user, initUser }}>{children}</UserContext.Provider>;
};
