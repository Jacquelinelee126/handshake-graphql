import { useState } from 'react';

export default () => {
    const [basic, setBasic] = useState({
        name:''
    });

    const [education, setEducation] = useState({

    });

    const [user, setUser] = useState({
        username: '',
        role: '',
        _id: '',
    });

    const updateBasic = (data) => {
        console.log('data in initUser', data);
        setBasic(data);
    };

    return {
        basic,
        updateBasic,
        education,
        updateEducation
    };
}; // 用自定义的hook去维护一个model，这个hook就可以被其他组件使用
