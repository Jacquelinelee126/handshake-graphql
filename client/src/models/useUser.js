import { useState } from 'react';

export default () => {

    const [user, setUser] = useState({
        username: '',
        role: '',
        _id: '',
    });

    const initUser = (data) => {
        console.log('data in initUser', data);
        setUser(data);
    };

    return {
        user,
        initUser,
    };
}; // 用自定义的hook去维护一个model，这个hook就可以被其他组件使用
