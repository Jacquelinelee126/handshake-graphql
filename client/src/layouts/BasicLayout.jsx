import React, { useEffect } from 'react';
import { Layout } from 'antd';
import { useUserContext } from '../common/context/UserContext';
import { useQuery } from '@apollo/react-hooks';
import * as queries from '../pages/user/queries';
import GlobalHeader from './GlobalHeader';
// import { effects } from "../pages/user/model";

import './index.css';

const { Content, Footer } = Layout;

export default function BasicLayout(props) {
    // const { user, initUser } = useUserContext();

    // console.log("user in basic layout", user);
    // const { INIT } = queries;

    // const {data, loading, error} = useQuery(INIT, 
    //   {
    //     variables: { user},
    //   },
    //   {
    //     errorPolicy: 'all',
    // });


    // console.log("data", data);

    // useEffect(() => {
    //     if (!user.username) {
    //         const res = init(user._id);

    //         console.log('res in basic layout', res);
    //     }
    // }, [user.username]);

    // if (loading) return <div>loading...</div>;
    // if (error) return <div>Error.</div>;

    return (
        <Layout className="basicLayout--container">
            <GlobalHeader />
            <Content className="basicLayout--content">
                <div className="site-layout-content">{props.children}</div>
            </Content>
            <Footer className="basicLayout--footer"></Footer>
        </Layout>
    );
}
