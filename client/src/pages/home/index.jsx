import React from 'react';
import { Button, Result, Card, Col, Row } from 'antd';
import { SmileOutlined } from '@ant-design/icons';
import { Link } from 'react-router-dom';
import BasicLayout from '../../layouts/BasicLayout';
import { useUserContext } from '../../common/context/UserContext';

export default function Routes(props) {
    const { user } = useUserContext();

    console.log('user in home', user);
    return (
        <BasicLayout>
            <Result icon={<SmileOutlined />} title={`${user.username}, Welcome to HandShake!`} />
            {user.role === 'student' ? (
                <Row gutter={16}>
                    <Col span={8}>
                        <Card title="Profile" bordered={false}>
                            <Button type="primary" shape="round">
                                <Link to="/student/profile">Go</Link>
                            </Button>
                        </Card>
                    </Col>
                    <Col span={8}>
                        <Card title="Jobs" bordered={false}>
                            <Button type="primary" shape="round">
                                <Link to="/search/job">Go</Link>
                            </Button>
                        </Card>
                    </Col>
                    <Col span={8}>
                        <Card title="Students" bordered={false}>
                            <Button type="primary" shape="round">
                                <Link to="/search/student">Go</Link>
                            </Button>
                        </Card>
                    </Col>
                    <Col span={8}>
                        <Card title="Applications" bordered={false}>
                            <Button type="primary" shape="round">
                                <Link to="/student/application">Go</Link>
                            </Button>
                        </Card>
                    </Col>
                   
                </Row>
            ) : (
                <Row gutter={16}>
                    <Col span={8}>
                        <Card title="Company Profile" bordered={false}>
                            <Button type="primary" shape="round">
                                <Link to="/company/profile">Go</Link>
                            </Button>
                        </Card>
                    </Col>
                    <Col span={8}>
                        <Card title="Post Jobs" bordered={false}>
                            <Button type="primary" shape="round">
                                <Link to="/company/job/create">Go</Link>
                            </Button>
                        </Card>
                    </Col>
                    <Col span={8}>
                        <Card title="Jobs" bordered={true}>
                            <Button type="primary" shape="round">
                                <Link to="/company/job">Go</Link>
                            </Button>
                        </Card>
                    </Col>

                    <Col span={8}>
                        <Card title="Students" bordered={false}>
                            <Button type="primary" shape="round">
                                <Link to="/search/student">Go</Link>
                            </Button>
                        </Card>
                    </Col>
                </Row>
            )}
        </BasicLayout>
    );
}
