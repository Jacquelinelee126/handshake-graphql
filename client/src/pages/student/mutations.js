import { gql } from 'apollo-boost';
// var { GraphQLYYYYMMDD } = require('graphql-moment');

const ADD_BASIC = gql`
    mutation(
        $name: String
        $dob: String
        $city: String
        $state: String
        $country: String
        $skillset: String
        $careerObjective: String
    ) {
        addBasic(
            dob: $dob
            name: $name
            city: $city
            state: $state
            country: $country
            skillset: $skillset
            careerObjective: $careerObjective
        ) {
            error
            message
        }
    }
`;

const UPDATE_BASIC = gql`
    mutation(
        $name: String
        $dob: String
        $city: String
        $state: String
        $country: String
        $skillset: String
        $careerObjective: String
    ) {
        updateBasic(
            name: $name
            dob: $dob
            city: $city
            state: $state
            country: $country
            skillset: $skillset
            careerObjective: $careerObjective
        ) {
            error
            message
        }
    }
`;

const ADD_EDUCATION = gql`
    mutation(
        $collegeName: String
        $major: String
        $location: String
        $degree: String
        $yearOfPassing: String
    ) {
        addEducation(
            collegeName: $collegeName
            major: $major
            location: $location
            degree: $degree
            yearOfPassing: $yearOfPassing
        ) {
            error
            message
        }
    }
`;

const UPDATE_EDUCATION = gql`
    mutation(
        $collegeName: String
        $major: String
        $location: String
        $degree: String
        $yearOfPassing: String
    ) {
        updateEducation(
            collegeName: $collegeName
            major: $major
            location: $location
            degree: $degree
            yearOfPassing: $yearOfPassing
        ) {
            error
            message
        }
    }
`;

const ADD_EXPERIENCE = gql`
    mutation(
        $companyName: String
        $title: String
        $location: String
        $startDate: String
        $endDate: String
        $description: String
    ) {
        addExperience(
            companyName: $companyName
            title: $title
            location: $location
            startDate: $startDate
            endDate: $endDate
            description: $description
        ) {
            error
            message
        }
    }
`;

const UPDATE_EXPERIENCE = gql`
    mutation(
        $expID: String
        $companyName: String
        $title: String
        $location: String
        $startDate: String
        $endDate: String
        $description: String
    ) {
        updateExperience(
            expID: $expID
            companyName: $companyName
            title: $title
            location: $location
            startDate: $startDate
            endDate: $endDate
            description: $description
        ) {
            error
            message
        }
    }
`;

export {
    ADD_BASIC,
    UPDATE_BASIC,
    ADD_EDUCATION,
    UPDATE_EDUCATION,
    ADD_EXPERIENCE,
    UPDATE_EXPERIENCE,
};
