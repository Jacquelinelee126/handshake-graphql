import React from 'react';
import { message as Messenger } from 'antd';
import { useMutation } from '@apollo/react-hooks';
import ExperienceForm from '../components/ExperienceForm';
import * as mutations from '../../../mutations';
import { useHistory } from 'react-router-dom';

export default function BasicPage() {
    let history = useHistory();

    const { ADD_EXPERIENCE } = mutations;

    const [addExperience] = useMutation(ADD_EXPERIENCE, {
        errorPolicy: 'all',
    });

    const handleCreate = async (values) => {
        try {
            console.log('values', values);
            const { data } = await addExperience({ variables: values });
            history.push('/student/profile');
            console.log('data', data);
            const res = data && data.addExperience;

            if (res.error) {
                console.log('res.message', res.message);
                Messenger.error('Adding experience failed. Please try again later.');
            } else {
                Messenger.success(res.message);
            }
        } catch (e) {
            console.log(e);
            Messenger.error('Network Error');
        }
    };

    return (
        <div>
            <ExperienceForm onSubmit={handleCreate} />
        </div>
    );
}
