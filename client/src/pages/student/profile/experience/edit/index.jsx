import React from 'react';
import { message as Messenger } from 'antd';

import { QUERY_EXPERIENCE_BY_ID } from '../../../queries';
import { UPDATE_EXPERIENCE } from '../../../mutations';
import { useQuery, useMutation } from '@apollo/react-hooks';
import { useHistory, useParams } from 'react-router-dom';

import ExperienceForm from '../components/ExperienceForm';

export default function () {
    const { expID } = useParams();
    console.log('expID', expID);
    let history = useHistory();
    const { data, loading, error } = useQuery(QUERY_EXPERIENCE_BY_ID, {
        variables: {
            expID: expID,
        },
    });

    console.log('data in edit', data);
    const currentExp = data && data.experience;
    console.log('current exp in edit', currentExp);

    const [updateExperience] = useMutation(UPDATE_EXPERIENCE, {
        errorPolicy: 'all',
    });

    console.log('updateExperience', updateExperience);
    const handleUpdate = async (values) => {
        try {
            values = { ...values, expID };

            console.log('values', values);
            const { data } = await updateExperience({ variables: values });
            history.push('/student/profile');
            console.log('data', data);
            const res = data && data.addExperience;

            if (res && res.error) {
                console.log('res.message', res.message);
                Messenger.error('Updating experience failed. Please try again later.');
            } else if (res && !res.error) {
                Messenger.success(res.message);
            }
        } catch (e) {
            console.log(e);
            Messenger.error('Network Error');
        }
    };

    if (loading) {
        return <div>loading...</div>;
    }

    if (error) {
        return <div>error</div>;
    }

    return (
        <div>
            <ExperienceForm initialValues={currentExp} onSubmit={handleUpdate} />
        </div>
    );
}
