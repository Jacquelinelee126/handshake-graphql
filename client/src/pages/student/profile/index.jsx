/**
 * @file student user profile page
 * @description
 * Display if user has created a profile otherwise
 * allow user to add one by clicking "create" button
 */

import React, { useEffect } from 'react';
// import { useSelector, useDispatch } from 'react-redux';
import { Button } from 'antd';
import { Link } from 'react-router-dom';
import { QUERY_BASIC, QUERY_EDUCATION, QUERY_EXPERIENCES } from '../queries';
import { useQuery } from '@apollo/react-hooks';

import Basic from './basic/components/Basic';
import Education from './education/components/Education';
import Experience from './experience/components/Experience';

const Profile = () => {
    const { data } = useQuery(QUERY_BASIC);

    console.log('data in profile', data);

    const basic = data && data.basic;
    console.log('basic in profile', basic);

    const educationRES = useQuery(QUERY_EDUCATION);

    const educationDATA = educationRES.data;

    const education = educationDATA && educationDATA.education;

    const experiencesRES = useQuery(QUERY_EXPERIENCES);

    const experiencesDATA = experiencesRES.data;

    const experiences = experiencesDATA && experiencesDATA.experiences;

    console.log('experiences', experiences);

    return (
        <div>
            <div className="basic">
                {basic ? (
                    <div>
                        <Basic {...basic} />
                        <Button type="primary">
                            <Link to="/student/profile/basic/edit">Edit</Link>
                        </Button>
                    </div>
                ) : (
                    <Button type="primary">
                        <Link to="/student/profile/basic/create">Create</Link>
                    </Button>
                )}
            </div>{' '}
            <div className="education">
                {education ? (
                    <div>
                        <Education {...education} />
                        <Button type="primary">
                            <Link to="/student/profile/education/edit">Edit</Link>
                        </Button>
                    </div>
                ) : (
                    <Button type="primary">
                        <Link to="/student/profile/education/create">Add Education Experience</Link>
                    </Button>
                )}
            </div>
            <div className="experiences">
                <div>
                    {experiences && <Experience experiences={experiences} />}
                </div>
                <br></br>
                <br></br>
                <Button type="primary">
                    <Link to="/student/profile/experience/create">Add Work Experience</Link>
                </Button>
            </div>{' '}
        </div>
    );
};

export default Profile;
