import React from 'react';
import { Form, Input, Select, Button } from 'antd';
import moment from 'moment';

const formItemLayout = {
    labelCol: {
        xs: {
            span: 24,
        },
        sm: {
            span: 8,
        },
    },
    wrapperCol: {
        xs: {
            span: 24,
        },
        sm: {
            span: 16,
        },
    },
};
const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 16,
            offset: 8,
        },
    },
};

const EducationForm = ({ onSubmit, initialValues = {} }) => {
    const [form] = Form.useForm();

    const onFinish = (values) => {
        onSubmit && onSubmit(values);
    };

    return (
        <div className="education-form">
            <Form
                {...formItemLayout}
                form={form}
                name="educatin_profile"
                onFinish={onFinish}
                initialValues={{ ...initialValues, dob: moment(initialValues.dob) }}
                scrollToFirstError
            >
                <Form.Item
                    label="College Name"
                    name="collegeName"
                    rules={[
                        {
                            required: true,
                            message: 'Please input college Name!',
                            whitespace: true,
                        },
                    ]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="Major"
                    name="major"
                    rules={[
                        {
                            required: true,
                            message: 'Please input the college major!',
                            whitespace: true,
                        },
                    ]}
                >
                    {/* Software Engineering, Electrical Engineering'),('Communications'),
        // ('Business'),
        // ('Economics'),
        // ('Literature'),
        // ('Psychology'),
        // ('Education'),
        // ('Nursing'),
        // ('Computer Science'),
        // ('All');*/}
                    <Select>
                        <Select.Option value="business">Business</Select.Option>
                        <Select.Option value="Commnication">Communications</Select.Option>
                        <Select.Option value="Education">Education</Select.Option>
                        <Select.Option value="Electrical Engineering">
                            Electrical Engineering
                        </Select.Option>
                        <Select.Option value="Nursing">Nursing</Select.Option>
                        <Select.Option value="Psychology">Psychology</Select.Option>
                        <Select.Option value="Software Engineering">
                            Software Engineering
                        </Select.Option>
                        <Select.Option value="Fashion Design">Fashion Design</Select.Option>
                        <Select.Option value="Marketing">Marketing</Select.Option>
                    </Select>
                </Form.Item>

                <Form.Item
                    label="Location"
                    name="location"
                    rules={[
                        {
                            required: true,
                            message: 'Please input the college location!',
                        },
                    ]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="Degree"
                    name="degree"
                    rules={[
                        {
                            required: true,
                            message: 'Please input your location!',
                            whitespace: true,
                        },
                    ]}
                >
                    <Select>
                        <Select.Option value="master">Master</Select.Option>
                        <Select.Option value="phd">PHD</Select.Option>
                        <Select.Option value="bachelor">Bachelor</Select.Option>
                    </Select>
                </Form.Item>

                <Form.Item
                    label="Year Of Passing"
                    name="yearOfPassing"
                    rules={[
                        {
                            required: true,
                            message: 'Please input when you graduated!',
                            whitespace: true,
                        },
                    ]}
                >
                    <Input />
                </Form.Item>
                <Form.Item {...tailFormItemLayout}>
                    <Button type="primary" htmlType="submit">
                        Submit
                    </Button>
                </Form.Item>
            </Form>
        </div>
    );
};

export default EducationForm;
