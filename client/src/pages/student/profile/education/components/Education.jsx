import React from "react";
import { Descriptions } from "antd";

export default function(props) {
  return (
    <Descriptions title="Education">
      <Descriptions.Item label="College">{props.collegeName}</Descriptions.Item>
      <Descriptions.Item label="Major">{props.major}</Descriptions.Item>
      <Descriptions.Item label="Location">{props.location}</Descriptions.Item>
      <Descriptions.Item label="Degree">{props.degree}</Descriptions.Item>
      <Descriptions.Item label="Year">{props.yearOfPassing}</Descriptions.Item>    
    </Descriptions>
  );
}
