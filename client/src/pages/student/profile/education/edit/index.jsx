import React, { useState } from 'react';
import { message } from 'antd';

import { QUERY_EDUCATION } from '../../../queries';
import { UPDATE_EDUCATION } from '../../../mutations';
import { useQuery, useMutation } from '@apollo/react-hooks';
import { useHistory } from 'react-router-dom';

import EducationForm from '../components/EducationForm';

export default function () {
    let history = useHistory();
    const { data, loading, error } = useQuery(QUERY_EDUCATION);
    console.log('data in edit', data);
    const education = data && data.education;

    console.log('Education in edit', education);

    const [updateEducation] = useMutation(UPDATE_EDUCATION, {
        errorPolicy: 'all',
    });

    console.log('updateEducation', updateEducation);
    const handleUpdate = async (values) => {
        try {
            console.log('Inside edit Education');
            await updateEducation({ variables: values });
            history.push('/student/profile');
            message.success('Successfully updated.');
        } catch (e) {
            console.log('error updating', e);
            message.error('Update failed.');
        }
    };

    if (loading) {
        return <div>loading...</div>;
    }

    if (error) {
        return <div>error</div>;
    }

    return (
        <div>
            <EducationForm initialValues={education} onSubmit={handleUpdate} />
        </div>
    );
}
