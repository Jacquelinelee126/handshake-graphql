import React from 'react';
import { message as Messenger } from 'antd';
import { useMutation } from '@apollo/react-hooks';
import EducationForm from '../components/EducationForm';
import * as mutations from '../../../mutations';
import { useHistory } from 'react-router-dom';

export default function EducationPage() {
    let history = useHistory();

    const { ADD_EDUCATION } = mutations;

    const [addEducation] = useMutation(ADD_EDUCATION, {
        errorPolicy: 'all',
    });

    const handleCreate = async (values) => {
        try {
            console.log('values', values);
            const res = await addEducation({ variables: values });
            history.push('/student/profile');
            console.log('res', res);

            Messenger.success('Add Education information success');
        } catch (e) {
            Messenger.error('Network Error');
        }
    };

    return (
        <div>
            <EducationForm onSubmit={handleCreate} />
        </div>
    );
}
