import React from 'react';
import { message as Messenger } from 'antd';
import { useMutation } from '@apollo/react-hooks';
import BasicForm from '../components/BasicForm';
import * as mutations from '../../../mutations';
import { useHistory } from 'react-router-dom';

import { useUserContext } from '../../../../../common/context/UserContext';

export default function BasicPage() {
    let history = useHistory();

    const { ADD_BASIC } = mutations;

    const [addBaisc, { data, error, loading }] = useMutation(ADD_BASIC, {
        errorPolicy: 'all',
    });

    const handleCreate = async (values) => {
        try {
            console.log('values', values);
            const res = await addBaisc({ variables: values });
            history.push('/student/profile');
            console.log('res', res);
            // initUser();
            Messenger.success('Add basic information success');
        } catch (e) {
            Messenger.error('Updating Failed.');
        }
    };

    return (
        <div>
            <BasicForm onSubmit={handleCreate} />
        </div>
    );
}
