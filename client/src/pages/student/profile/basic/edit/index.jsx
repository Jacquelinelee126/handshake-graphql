import React, { useEffect } from 'react';
import { message } from 'antd';

import { QUERY_BASIC } from '../../../queries';
import { UPDATE_BASIC } from '../../../mutations';
import { useQuery, useMutation } from '@apollo/react-hooks';
import { useHistory } from 'react-router-dom';

import BasicForm from '../components/BasicForm';

export default function () {
    let history = useHistory();
    const { data, loading, error } = useQuery(QUERY_BASIC);
    console.log('data in edit', data);
    const basic = data && data.basic;
    console.log('basic in edit', basic);

    const [updateBasic] = useMutation(UPDATE_BASIC, {
        errorPolicy: 'all',
    });

    console.log('updatebasic', updateBasic);
    const handleUpdate = async (values) => {
        try {
            console.log('Inside edit basic');
            await updateBasic({ variables: values });
            history.push('/student/profile');
            message.success('Successfully updated.');
        } catch (e) {
            console.log('error updating', e);
            message.error('Update failed.');
        }
    };

    if (loading) {
        return <div>loading...</div>;
    }

    if (error) {
        return <div>error</div>;
    }

    return (
        <div>
            <BasicForm initialValues={basic} onSubmit={handleUpdate} />
        </div>
    );
}
