import React, { useState, useEffect } from 'react';
import { Table } from 'antd';
import { Link } from 'react-router-dom';

import { QUERY_APPLICATIONS } from '../queries';

import { useQuery } from '@apollo/react-hooks';
import moment from 'moment';

export default function ApplicationsPage() {
    const { data } = useQuery(QUERY_APPLICATIONS);

    const applications = data && data.applicationsStudent;

    console.log('applications ====> ', applications);

    const columns = [
        {
            title: 'Job Title',
            dataIndex: 'job',
            key: 'title',
            render: (record) =>
                <Link to={`/student/job/${record._id}`}>{record.title}</Link>,
        },
        {
            title: 'Applied Date',
            dataIndex: 'createdAt',
            key: 'appliedDate',
            render: (timestamp) => moment(timestamp, 'x').format('YYYY-MM-DD'),
        },
        {
            title: 'Status',
            dataIndex: 'status',
            key: 'status',
        },
    ];

    return (
        <>
            <Table
                key="_id"
                rowKey="id"
                columns={columns}
                dataSource={applications}
                pagination={false}
            />
            {/* <Pagination
                size="large"
                total={total}
                showTotal={(total) => `Total ${total} applications`}
                defaultCurrent={1}
                defaultPageSize={2}
                showQuickJumper={true}
                showSizeChanger={true}
                pageSizeOptions={[1, 2, 5, 10, 20]}
                onChange={onChange}
                onShowSizeChange={onChange}
            /> */}
        </>
    );
}
