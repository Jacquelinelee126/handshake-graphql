/**
 * @file student user profile page
 * @description
 * Display if user has created a profile otherwise
 * allow user to add one by clicking "create" button
 */
import { Descriptions, message, Button } from 'antd';
import { Link, useParams } from 'react-router-dom';

import React, { useEffect } from 'react';
import { useState } from 'react';
import { QUERY_STUDENT } from './queries';
import { useQuery } from '@apollo/react-hooks';

import Basic from '../student/profile/basic/components/Basic';
import Education from '../student/profile/education/components/Education';
// import Experience from '../student/profile/experience/components/Experience';

// import * as api from '../search/service';

const OneStudentProfile = () => {
    const { sID } = useParams();
    console.log('One SID:', sID);

    const [currentStudent, setCurrentStudent] = useState();
    // const [loading, setLoading] = useState(false); // loading

    const { data, loading, error } = useQuery(QUERY_STUDENT, {
        variables: {
            sID,
        },
    });

    if (loading) {
        return <div>loading...</div>;
    }
    if (error) {
        return <div>Error...</div>;
    }

    const student = data && data.studentProfile;
    console.log('student', student);

    const { basic, education } = student && student;
    // try catch

    // if (loading) {
    //     return <div>loading...</div>;
    // }
    // if (!loading) {
    //     return <div>Error...</div>;
    // }

    if (!student || Object.keys(student).length === 0) {
        return <h1>This student hasn't uploaded his profile yet!</h1>;
    }
    // reuse component and check user id

    return (
        <div>
            <Basic {...basic}></Basic>
            <Education {...education}></Education>
        </div>
    );
};

export default OneStudentProfile;
