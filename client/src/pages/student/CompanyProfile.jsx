/**
 * @file Company user profile page
 * @description
 * Display if user has created a profile otherwise
 * allow user to add one by clicking "create" button
 */

import { useParams } from 'react-router-dom';

import React from 'react';
import { useState } from 'react';
import { useQuery } from '@apollo/react-hooks';

import ProfileBasic from '../company/profile/component/ProfileBasic';
import { QUERY_COMPANY } from './queries';

const OneCompanyProfile = () => {
    const { company } = useParams();

    let [currentCompany, setCurrentCompany] = useState();
    
    const { data, loading, error } = useQuery(QUERY_COMPANY, {
        variables: { company },
    });

    currentCompany = data && data.companyProfile;

    if (loading) {
        return <div>loading...</div>;
    }
    if (error) {
        return <div>Error...</div>;
    }

    console.log('currentCompany', currentCompany);
    if (!currentCompany || Object.keys(currentCompany).length === 0) {
        return <h1>This Company no detailed information!</h1>;
    }
    // reuse component and check user id

    return (
        <div>
            <ProfileBasic {...currentCompany}></ProfileBasic>
        </div>
    );
};

export default OneCompanyProfile;
