/**
 * @file Job user profile page
 * @description
 * Display if user has created a profile otherwise
 * allow user to add one by clicking "create" button
 */

import React, { useState } from 'react';
import { useParams } from 'react-router-dom';
import JobDetail from '../company/job/component/JobDetail';
import { useQuery } from '@apollo/react-hooks';
import { QUERY_JOB_BY_ID } from '../company/queries';

const OneJobProfile = () => {
    const { jID } = useParams();
    console.log('One jID:', jID);

    let [currentJob] = useState();

    const { data, loading, error } = useQuery(QUERY_JOB_BY_ID, {
        variables: { jID },
    });

    currentJob = data && data.job;

    if (loading) {
        return <div>loading...</div>;
    }
    if (error) {
        return <div>Error...</div>;
    }

    console.log('currentJob', currentJob);

    if (!currentJob || Object.keys(currentJob).length === 0) {
        return <h1>This Job no detailed information!</h1>;
    }

    return (
        <div>
            <JobDetail {...currentJob}></JobDetail>
        </div>
    );
};

export default OneJobProfile;
