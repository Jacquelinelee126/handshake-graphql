import { gql } from 'apollo-boost';

const QUERY_BASIC = gql`
    query {
        basic {
            name
            dob
            city
            state
            country
            skillset
            careerObjective
        }
    }
`;

const QUERY_EDUCATION = gql`
    query {
        education {
            collegeName
            major
            location
            degree
            yearOfPassing
        }
    }
`;

const QUERY_EXPERIENCES = gql`
    query {
        experiences {
            _id
            companyName
            title
            location
            startDate
            endDate
        }
    }
`;

const QUERY_EXPERIENCE_BY_ID = gql`
    query($expID: String) {
        experience(expID: $expID) {
            _id
            companyName
            title
            location
            startDate
            endDate
        }
    }
`;

const QUERY_APPLICATIONS = gql`
    query {
        applicationsStudent {
            job {
                title
                _id
            }
            status
            createdAt
        }
    }
`;
const QUERY_COMPANY = gql`
    query($company: String) {
        companyProfile(company: $company) {
            name
            location
            description
            email
            phone
        }
    }
`;

const QUERY_STUDENT = gql`
    query($sID: String) {
        studentProfile(sID: $sID) {
            basic {
                name
                dob
                city
                state
                country
                skillset
                careerObjective
            }
            education {
                collegeName
                major
                location
                degree
                yearOfPassing
            }
        }
    }
`;

export {
    QUERY_BASIC,
    QUERY_EDUCATION,
    QUERY_EXPERIENCES,
    QUERY_EXPERIENCE_BY_ID,
    QUERY_APPLICATIONS,
    QUERY_COMPANY,
    QUERY_STUDENT,
};
