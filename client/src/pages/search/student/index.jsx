import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Table, Pagination, message } from 'antd';
// import { useSelector } from "react-redux";

import { SEARCH_STUDENTS } from '../queries';
import { useQuery } from '@apollo/react-hooks';
import { useHistory } from 'react-router-dom';

import SearchForm from './SearchForm';
import BasicLayout from '../../../layouts/BasicLayout';

export default function SearchPage(props) {
    let [students, setState] = useState({ students: [], total: 0 });
    const [{ searchBy, keyword, major }, setCombinations] = useState({});
    const { data, loading: jobLoading, error: jobError } = useQuery(SEARCH_STUDENTS, {
        variables: {
            searchBy,
            keyword,
            major,
        },
    });

    students = data && data.students;

    const handleSubmit = async (values) => {
        console.log('values', values);
        setCombinations(values);
    };

    const columns = [
        {
            title: 'Name',
            dataIndex: 'basic',
            key: 'title',
            render: (basic, record) =>
                basic ? <Link to={`/student/profile/${record._id}`}>{record.basic.name}</Link> : 'N/A',
        },
        {
            title: 'Major',
            dataIndex: 'education',
            key: 'major',
            render: (education) => (education ? education.major : 'N/A'),
        },
        {
            title: 'College',
            dataIndex: 'education',
            key: 'collegeName',
            render: (education) => (education ? education.collegeName : 'N/A'),
        },
    ];

    return (
        <BasicLayout>
            <SearchForm onSubmit={handleSubmit} />
            <Table rowKey="_id" columns={columns} dataSource={students} pagination={true} />
        </BasicLayout>
    );
}
