import { gql } from 'apollo-boost';

const SEARCH_JOBS = gql`
    query($searchBy: String, $keyword: String, $category: String, $location: String) {
        jobsStudent(
            searchBy: $searchBy
            keyword: $keyword
            category: $category
            location: $location
        ) {
            _id
            title
            location
            salary
            applicationDeadline
            company {
                name
            }
        }
    }
`;

const SEARCH_STUDENTS = gql`
    query($searchBy: String, $keyword: String, $major: String) {
        students(searchBy: $searchBy, keyword: $keyword, major: $major) {
            _id
            basic {
                name
            }
            education {
                collegeName
                major
            }
        }
    }
`;

export { SEARCH_JOBS, SEARCH_STUDENTS };
