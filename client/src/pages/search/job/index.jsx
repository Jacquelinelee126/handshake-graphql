import React, { useState, useEffect } from 'react';
import { Table, Popconfirm, message as Messenger, Button } from 'antd';
import { Link } from 'react-router-dom';
import { SEARCH_JOBS } from '../queries';
import { APPLY_JOB } from '../mutations';
import { useQuery, useMutation } from '@apollo/react-hooks';

import SearchForm from './SearchForm';
import BasicLayout from '../../../layouts/BasicLayout';

import moment from 'moment';

export default function SearchPage(props) {
    let [jobs, setJobs] = useState([]);

    const [{ searchBy, keyword, category, location }, setCombinations] = useState({});

    const [applyJob] = useMutation(APPLY_JOB, {
        errorPolicy: 'all',
    });

    const { data, loading: jobLoading, error: jobError } = useQuery(SEARCH_JOBS, {
        variables: {
            searchBy,
            keyword,
            category,
            location,
        },
    });

    jobs = data && data.jobsStudent;

    const handleSubmit = (values) => {
        console.log('values', values);
        setCombinations(values);
    };

    const handleApply = async (record) => {
        try {
            console.log('record id', record);
            const { data } = await applyJob({
                variables: {
                    jID: record._id,
                },
            });

            const res = data && data.applyJob;

            if (res.error) {
                Messenger.error(res.message);
            } else {
                Messenger.success(res.message);
            }
        } catch (e) {
            Messenger.error('Network Error');
        }
    };

    const columns = [
        {
            title: 'Title',
            dataIndex: 'title',
            key: 'title',
            render: (text, record) => <Link to={`/student/job/${record._id}`}>{text}</Link>,
        },
        {
            title: 'Company',
            dataIndex: 'company',
            key: 'company',
            render: (company) =>
                company ? (
                    <Link to={`/student/company/${company.name}`}>{company.name} </Link>
                ) : (
                    'N/A'
                ),
        },
        {
            title: 'Location',
            dataIndex: 'location',
            key: 'location',
        },
        {
            title: 'Salary',
            dataIndex: 'salary',
            key: 'salary',
        },
        {
            title: 'job desc',
            dataIndex: 'job_description',
            key: 'job_description',
        },
        {
            title: 'application_deadline',
            dataIndex: 'applicationDeadline',
            key: 'application_deadline',
            render: (timestamp) => moment(timestamp, 'x').format('YYYY-MM-DD'),
        },
        {
            title: 'Action',
            key: 'action',
            render: (text, record) => (
                <Popconfirm
                    title="Want to apply?"
                    onConfirm={() => handleApply(record)}
                    okText="Yes"
                    cancelText="No"
                >
                    <Button>apply</Button>
                </Popconfirm>
            ),
        },
    ];

    return (
        <BasicLayout>
            <SearchForm onSubmit={handleSubmit} />
            <Table rowKey="id" columns={columns} dataSource={jobs} />
        </BasicLayout>
    );
}
