import { gql } from 'apollo-boost';

const APPLY_JOB = gql`
    mutation($jID: String) {
        applyJob(jID: $jID) {
            error
            message
        }
    }
`;

export { APPLY_JOB };
