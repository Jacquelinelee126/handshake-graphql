import React from 'react';
import { Form, Input, Tooltip, Select, Button, message as Messenger } from 'antd';
import { QuestionCircleOutlined } from '@ant-design/icons';
import { useMutation } from '@apollo/react-hooks';
import { useHistory } from 'react-router-dom';

import * as mutations from '../mutations';

// import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
// import { effects } from '../model';

import './index.css';

const formItemLayout = {
    labelCol: {
        xs: {
            span: 24,
        },
        sm: {
            span: 8,
        },
    },
    wrapperCol: {
        xs: {
            span: 24,
        },
        sm: {
            span: 16,
        },
    },
};
const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 16,
            offset: 8,
        },
    },
};

const RegistrationForm = () => {
    let history = useHistory();

    const [form] = Form.useForm();
    const { USER_REGISTRATION } = mutations;
    // const dispatch = useDispatch();
    // const { loading, error, data } = useQuery(USER_REGISTRATION);
    const [userRegistration, { data, error, loading }] = useMutation(USER_REGISTRATION, {
        errorPolicy: 'all',
    });
    console.log('data register', data);
    console.log('errors register, ', typeof error);

    const onFinish = async (values) => {
        console.log('Received values of form: ', values);
        const { data } = await userRegistration({
            variables: values,
        });

        const res = data && data.userRegistration;
        console.log('res.error', res);
        if (res.error) Messenger.error(res.message);
        else {
            Messenger.success(res.message);
            history.push('/user/login');
        }
    };

    const onFinishFailed = (errorInfo) => {
        Messenger.error('Failed:', errorInfo);
        console.log('Failed:', errorInfo);
    };

    if (loading) {
        return <div>Loading data...</div>;
    }
    if (error) {
        return <div>Error occurred</div>;
    }

    return (
        <div className="register-form">
            <Form
                {...formItemLayout}
                form={form}
                name="register"
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
                initialValues={{}}
                scrollToFirstError
            >
                <Form.Item
                    name="username"
                    label={
                        <span>
                            Username&nbsp;
                            <Tooltip title="What do you want others to call you?">
                                <QuestionCircleOutlined />
                            </Tooltip>
                        </span>
                    }
                    rules={[
                        {
                            required: true,
                            Messenger: 'Please input your nickname!',
                            whitespace: true,
                        },
                    ]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    name="email"
                    label="E-mail"
                    rules={[
                        {
                            type: 'email',
                            Messenger: 'The input is not valid E-mail!',
                        },
                        {
                            required: true,
                            Messenger: 'Please input your E-mail!',
                        },
                    ]}
                >
                    <Input />
                </Form.Item>
                <Form.Item label="Role" name="role">
                    <Select>
                        <Select.Option value="student">student</Select.Option>
                        <Select.Option value="company">company</Select.Option>
                    </Select>
                </Form.Item>
                <Form.Item
                    name="password"
                    label="Password"
                    rules={[
                        {
                            required: true,
                            Messenger: 'Please input your password!',
                        },
                    ]}
                    hasFeedback
                >
                    <Input.Password />
                </Form.Item>

                <Form.Item
                    name="passwordConfirmation"
                    label="Confirm Password"
                    dependencies={['password']}
                    hasFeedback
                    rules={[
                        {
                            required: true,
                            Messenger: 'Please confirm your password!',
                        },
                        ({ getFieldValue }) => ({
                            validator(rule, value) {
                                if (!value || getFieldValue('password') === value) {
                                    return Promise.resolve();
                                }

                                return Promise.reject(
                                    'The two passwords that you entered do not match!',
                                );
                            },
                        }),
                    ]}
                >
                    <Input.Password />
                </Form.Item>

                <Form.Item {...tailFormItemLayout}>
                    <Button type="primary" htmlType="submit">
                        Register
                    </Button>
                    <div>
                        Or <Link to="/user/login">go to login!</Link>
                    </div>
                </Form.Item>
            </Form>
        </div>
    );
};

export default RegistrationForm;
