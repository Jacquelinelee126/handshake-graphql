import { gql } from 'apollo-boost';

const GET_USER_BY_ID = gql`
    query($id: String) {
        getUser(_id: $id) {
            username
            role
            id
        }
    }
`;

const INIT = gql`
    query {
        currentUser {
            username
            role
            _id
        }
    }
`;

export { INIT, GET_USER_BY_ID };
