import { gql } from 'apollo-boost';

const USER_REGISTRATION = gql`
    mutation ($username: String, $email: String, $password: String, $role: String) {
        userRegistration(username: $username, email: $email, password: $password, role: $role) {
          error,
          message
        }
    }
`;

const USER_LOGIN = gql`
  mutation ($email: String, $password: String){
    userLogin(email: $email, password: $password){
      message
    }
  }
`;
export { USER_REGISTRATION, USER_LOGIN };
