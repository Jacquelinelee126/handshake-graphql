import React, { useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { useUserContext } from '../../../common/context/UserContext';

const Logout = () => {
    let history = useHistory();
    const { initUser } = useUserContext();

    useEffect(() => {
        localStorage.removeItem('gqltoken');
        initUser({})
        history.push('/user/login');
    });

    return <div>logout...</div>;
};

export default Logout;
