import React, { useState } from 'react';
import { Form, Input, Button, message as Messenger } from 'antd';
import jwtDecode from 'jwt-decode';

// import { connect } from 'react-redux';
// import { bindActionCreators } from 'redux';
import { useHistory } from 'react-router-dom';
import { Link } from 'react-router-dom';
import { useMutation } from '@apollo/react-hooks';
import * as mutations from '../mutations';
import { useUserContext } from '../../../common/context/UserContext';

// import { effects } from '../model';

import './index.css';
import { from } from 'apollo-boost';

const layout = {
    labelCol: {
        span: 8,
    },
    wrapperCol: {
        span: 16,
    },
};
const tailLayout = {
    wrapperCol: {
        offset: 8,
        span: 16,
    },
};

const NormalLoginForm = (props) => {
    const { initUser } = useUserContext();
    let [user, setUser] = useState({});
    let history = useHistory();

    const [form] = Form.useForm(); //
    const { USER_LOGIN } = mutations;

    // useMutation returns a userLogin function
    // it can be use when the form is finished
    const [login, { loading, error }] = useMutation(USER_LOGIN, {
        errorPolicy: 'all',
    });

    console.log('error: ', error);

    if (loading) {
        return <div>Loading data...</div>;
    }
    // if (error) {
    //     return Messenger.error(error);
    // }

    const onFinish = async (values) => {
        console.log('Success:', values);

        try {
            const res = await login({
                variables: values,
            });

            console.log('res', res);
            const { message } = res.data.userLogin;
            const token = message;
            localStorage.setItem('gqltoken', token);
            user = jwtDecode(token);
            initUser(user);

            Messenger.success('Login successfully');
            history.push('/home');
        } catch (e) {
            console.log(e);
            Messenger.error('Please check your credentials');
            history.push('/user/login');
        }
    };

    const onFinishFailed = (errorInfo) => {
        Messenger.error('Failed:', errorInfo);
        console.log('Failed:', errorInfo);
    };

    return (
        <div className="login-form">
            <Form
                form={form}
                {...layout}
                name="basic"
                initialValues={{
                    remember: true,
                }}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
            >
                <Form.Item
                    label="Email"
                    name="email"
                    rules={[
                        {
                            type: 'email',
                            Messenger: 'The input is not valid E-mail!',
                        },
                        {
                            required: true,
                            message: 'Please input your email!',
                        },
                    ]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="Password"
                    name="password"
                    rules={[
                        {
                            required: true,
                            message: 'Please input your password!',
                        },
                    ]}
                >
                    <Input.Password />
                </Form.Item>

                <Form.Item {...tailLayout}>
                    <Button type="primary" htmlType="submit">
                        Login
                    </Button>
                    <div>
                        Or <Link to="/user/register">register now!</Link>
                    </div>
                </Form.Item>
            </Form>
        </div>
    );
};

export default NormalLoginForm;
