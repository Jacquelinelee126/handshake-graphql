import React from "react";
import { Descriptions } from "antd";

export default function(props) {
  return (
    <Descriptions title="Basic Information" bordered
    column={{ xxl: 4, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}>
      <Descriptions.Item label="Name">{props.name}</Descriptions.Item>
      <Descriptions.Item label="Location">{props.location}</Descriptions.Item>
      <Descriptions.Item label="Email">{props.email}</Descriptions.Item>
      <Descriptions.Item label="Phone">{props.phone}</Descriptions.Item>
      <Descriptions.Item label="Description">{props.description}</Descriptions.Item>
    </Descriptions>
  );
}