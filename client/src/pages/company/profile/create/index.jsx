import React from 'react';
import { message as Messenger } from 'antd';
import { useMutation } from '@apollo/react-hooks';
import ProfileForm from '../component/ProfileForm';
import * as mutations from '../../mutations';
import { useHistory } from 'react-router-dom';

export default function BasicPage() {
    let history = useHistory();
    const { ADD_COMPANY_BASIC } = mutations;

    const [addCompanyBasic] = useMutation(ADD_COMPANY_BASIC, {
        errorPolicy: 'all',
    });

    const handleCreate = async (values) => {
        try {
            console.log('values', values);
            const res = await addCompanyBasic({ variables: values });
            history.push('/company/profile');
            console.log('res', res);
            Messenger.success('Add basic information success');
        } catch (e) {
            Messenger.error('Updating Failed.');
        }
    };

    return (
        <div>
            <ProfileForm onSubmit={handleCreate} />
        </div>
    );
}
