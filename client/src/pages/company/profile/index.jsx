import React from 'react';
import { Button, Empty } from 'antd';
import { Link } from 'react-router-dom';
import { QUERY_COMPANY_BASIC } from '../queries';
import { useQuery } from '@apollo/react-hooks';
import ProfileBasic from './component/ProfileBasic';

const Profile = () => {
  const { data, error, loading} = useQuery(QUERY_COMPANY_BASIC);

  console.log('data in profile', data);

  const company = data && data.companyBasic;
  console.log('basic in profile', company);
  
    if (loading) {
        return <div>loading...</div>;
    }

    if (error) {
        return <div>error...</div>;
    }

    if (!company) {
        return (
            <div>
                <Empty
                    image="https://gw.alipayobjects.com/zos/antfincdn/ZHrcdLPrvN/empty.svg"
                    imageStyle={{
                        height: 60,
                    }}
                    description={
                        <span>
                           It looks like your profile is empty.
                        </span>
                    }
                >
                    <Button type="primary">
                        <Link to="/company/profile/create">Create Now!</Link>
                    </Button>
                </Empty>
                {/* <h3> It looks like your profile is empty. Please create it!</h3>
                <Button type="primary">
                    <Link to="/company/profile/create">Create</Link>
                </Button> */}
            </div>
        );
    }

    return (
        <div>
            <ProfileBasic {...company} />
            {/* <ProfileBasic {...company} /> */}
            <Button type="primary">
                <Link to="/company/profile/edit">Edit</Link>
            </Button>
        </div>
    );
};

export default Profile;
