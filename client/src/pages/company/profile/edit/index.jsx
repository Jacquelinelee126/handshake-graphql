import React, { useEffect } from 'react';
import { message as Messenger } from 'antd';

import { QUERY_COMPANY_BASIC } from '../../queries';
import { UPDATE_COMPANY_BASIC } from '../../mutations';
import { useQuery, useMutation } from '@apollo/react-hooks';
import { useHistory } from 'react-router-dom';

import ProfileForm from '../component/ProfileForm';

export default function () {
    let history = useHistory();
    const { data, loading, error } = useQuery(QUERY_COMPANY_BASIC);
    console.log('data in edit', data);
    const company = data && data.companyBasic;

    const [updateCompanyBasic] = useMutation(UPDATE_COMPANY_BASIC, {
        errorPolicy: 'all',
    });

    const handleUpdate = async (values) => {
        try {
            const { data } = await updateCompanyBasic({
                variables: values,
            });

            const res = data && data.updateCompanyBasic;
            console.log('res.error', res);

            if (res.error) {
                Messenger.error(res.message);
            } else {
                Messenger.success(res.message);
                history.push('/company/profile');
            }
        } catch (e) {
            console.log('error updating', e);
            Messenger.error('Update failed.');
        }
    };

    if (loading) {
        return <div>loading...</div>;
    }

    if (error) {
        return <div>error</div>;
    }

    return (
        <div>
            <ProfileForm initialValues={company} onSubmit={handleUpdate} />
        </div>
    );
}
