import React, { useEffect } from 'react';
import { Button } from 'antd';
import { QUERY_JOBS_COMPANY } from '../queries';
import { useQuery } from '@apollo/react-hooks';

import { Link } from 'react-router-dom';

import JobList from './component/JobList';

const Job = () => {
    const { data, loading, error } = useQuery(QUERY_JOBS_COMPANY);

    const jobs = data && data.jobsCompany;

    console.log("jobs",jobs);
    if (loading) {
        return <div>loading...</div>;
    }

    if (error) {
        return <div>error</div>;
    }

    if (jobs.length === 0) {
        return (
            <div>
                <h3> You don't have any job, please create it!</h3>
                <Button type="primary">
                    <Link to="/company/job/create">Create</Link>
                </Button>
            </div>
        );
    }

    return (
        <div>
            {/* job list*/}
            {/* key={number.toString()} */}
            <JobList list={jobs} />
        </div>
    );
};

export default Job;
