import React, { useEffect } from 'react';
import { message as Messenger } from 'antd';
import { useMutation, useQuery} from '@apollo/react-hooks';
import { QUERY_JOB_BY_ID } from '../../queries';
import { UPDATE_JOB } from '../../mutations';

import JobForm from '../component/JobForm';
import { useHistory } from 'react-router-dom';
import { useParams } from 'react-router-dom';

const EditJobPage = () => {
    let history = useHistory();
    const { jID } = useParams();

    console.log("jId",jID)
    
    const { data, loading, error } = useQuery(QUERY_JOB_BY_ID, {
        variables: {
            jID: jID,
        },
    });
    const currentJob = data && data.job;
    const [updateJob] = useMutation(UPDATE_JOB, {
        errorPolicy: 'all',
    });

    const handleUpdate = async (values) => {
        try {
           
            values = {...values, jID};
            console.log('Inside edit exp, values', values);
            const {data} = await updateJob({variables: values});

            const res = data && data.updateJob;

            if (res.error) {
                Messenger.error(res.message);
            } else {
                Messenger.success(res.message);
                history.push('/company/job');
            }
        } catch (e) {
            console.log('error updating', e);
            Messenger.error('Update failed.');
        }
    };

    if (loading) {
        return <div>loading...</div>;
    }

    if (error) {
        return <div>error</div>;
    }

    return (
        <div>
            <JobForm initialValues={currentJob} onSubmit={handleUpdate} />
        </div>
    );
};

export default EditJobPage;
