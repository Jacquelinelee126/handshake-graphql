import React from 'react';
import { message as Messenger } from 'antd';
import { useMutation } from '@apollo/react-hooks';
import JobForm from '../component/JobForm';
import { ADD_JOB } from '../../mutations';
import { useHistory } from 'react-router-dom';

export default function () {
    let history = useHistory();
    const [addJob] = useMutation(ADD_JOB, {
        errorPolicy: 'all',
    });

    const handleCreate = async (values) => {
        try {
            const { data } = await addJob({ variables: values });

            const res = data && data.addJob;
            console.log('res.error', res);

            if (res.error) {
                Messenger.error(res.message);
            } else {
                Messenger.success(res.message);
                history.push('/company/job');
            }
        } catch (e) {
            Messenger.error('Updating Failed.');
        }
    };

    return (
        <div>
            <JobForm onSubmit={handleCreate} />
        </div>
    );
}
