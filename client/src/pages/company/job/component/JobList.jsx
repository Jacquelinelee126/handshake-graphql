import React from 'react';
import { Table, Divider } from 'antd';
import { Link } from 'react-router-dom';
import moment from 'moment';

export default function (props) {
    const columns = [
        {
            title: 'Title',
            dataIndex: 'title',
            key: 'title',
        },
        {
            title: 'Location',
            dataIndex: 'location',
            key: 'location',
        },
        {
            title: 'Salary',
            dataIndex: 'salary',
            key: 'salary',
        },
        {
            title: 'Description',
            dataIndex: 'jobDescription',
            key: 'jobDescription',
        },
        {
            title: 'Application Dealine',
            dataIndex: 'applicationDeadline',
            key: 'applicationDealine',
            render: (timestamp) => moment(timestamp, 'x').format('YYYY-MM-DD'),
        },
        {
            title: 'Actions',
            key: 'action',
            render: (record) => (
                <span>
                    <Link to={`/company/job/${record._id}`}>modify</Link>
                    <Divider type="vertical" />
                    <Link to={`/company/job/${record._id}/application`}>applications</Link>
                </span>
            ),
        },
    ];

    return <Table rowKey="_id" dataSource={props.list} columns={columns} />;
}
