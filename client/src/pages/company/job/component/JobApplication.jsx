import React, { useState } from 'react';
import { Table, Button, message as Messenger } from 'antd';

import { Link } from 'react-router-dom';
import ApplicationStatusForm from './ApplicationStatusForm';
import { useMutation } from '@apollo/react-hooks';
import { UPDATE_APPLICATION_STATUS } from '../../mutations';

export default function (props) {
    console.log('props,', props.list);
    const [visible, setVisible] = useState(false);
    const [current, setCurrent] = useState();
    const [changeApplicationStatus] = useMutation(UPDATE_APPLICATION_STATUS);
    const columns = [
        {
            title: 'Applicant',
            dataIndex: 'student',
            key: 'applicant',
            // render:(student)  => student? 'TEST' : 'N/A'
            render: (student) =>
                student ? (
                    <Link to={`/student/profile/${student.owner}`}>{student.name}</Link>
                ) : (
                    'N/A'
                ),
        },

        {
            title: 'status',
            dataIndex: 'status',
            key: 'status',
        },
        {
            title: 'Action',
            key: 'action',
            render: (text, record) => (
                <span>
                    <Button
                        onClick={() => {
                            setCurrent(record);
                            setVisible(true);
                        }}
                    >
                        Change Status
                    </Button>
                </span>
            ),
        },
    ];

    const handleSubmit = async (values) => {
        console.log('handleSubmit:', current);
        console.log('current:', current._id);
        const { _id } = current;

        console.log('values', values);
        try {
            values = { ...values, aID: _id };
            console.log('values', values);

            const { data } = await changeApplicationStatus({ variables: values });

            const res = data && data.changeApplicationStatus;

            if (res.error) {
                Messenger.error('Update failed. Please try again later.');
            } else {
                Messenger.success(res.message);
            }
        } catch (e) {
            Messenger.error('Network Error');
        }
    };

    const handleCancel = () => {
        setVisible(false);
        setCurrent(null);
    };

    return (
        <>
            <Table rowKey="id" dataSource={props.list} columns={columns} pagination={false} />
            <ApplicationStatusForm
                visible={visible}
                initial={current}
                onSubmit={handleSubmit}
                onCancel={handleCancel}
            />

            {/* <Pagination
                size="large"
                total={total}
                showTotal={(total) => `Total ${total} applications`}
                defaultCurrent={1}
                defaultPageSize={2}
                showQuickJumper={true}
                showSizeChanger={true}
                pageSizeOptions={[1, 2, 5, 10, 20]}
                onChange={onChange}
                onShowSizeChange={onChange}
            /> */}
        </>
    );
}
