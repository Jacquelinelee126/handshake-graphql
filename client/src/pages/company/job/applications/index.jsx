/**
 * @file List of applicatoins for one job
 * @description
 */
import React, { useState } from 'react';
import { Button } from 'antd';
import { QUERY_APPLICATIONS_FOR_ONE_JOB } from '../../queries';
import { useQuery } from '@apollo/react-hooks';
import { Link, useParams } from 'react-router-dom';

import JobApplication from '../component/JobApplication';

const JobApplicationPage = () => {
    let [applications] = useState([]);
    console.log('Application Page');
    const { jID } = useParams();

    console.log('jID', jID);

    const { data, loading, error } = useQuery(QUERY_APPLICATIONS_FOR_ONE_JOB, {
        variables: {
            jID: jID,
        },
    });

    applications = data && data.applicationsForOneJob;

    if (loading) {
        return <div>loading...</div>;
    }

    if (error) {
        return <div>error</div>;
    }

    if (applications.length === 0) {
        return (
            <div>
                <h3> This job has no application!</h3>
                <Button type="primary">
                    <Link to="/company/job">Go Back</Link>
                </Button>
            </div>
        );
    }

    return (
        <div>
            {/* application list*/}
            <JobApplication list={applications} jID={jID} />
        </div>
    );
};

export default JobApplicationPage;
