import { gql } from 'apollo-boost';

const QUERY_COMPANY_BASIC = gql`
    query {
        companyBasic {
            name
            location
            description
            email
            phone
        }
    }
`;

const QUERY_JOBS_COMPANY = gql`
    query {
        jobsCompany {
            _id
            title
            location
            salary
            jobDescription
            applicationDeadline
            category
        }
    }
`;

const QUERY_APPLICATIONS_FOR_ONE_JOB = gql`
    query($jID: String) {
        applicationsForOneJob(jID: $jID) {
            student {
                name
            }
            _id
            status
            createdAt
        }
    }
`;

const QUERY_JOB_BY_ID = gql`
    query($jID: String) {
        job(jID: $jID) {
            title
            location
            salary
            jobDescription
            applicationDeadline
            category
        }
    }
`;

const QUERY_APPLICATION_FOR_ONE_JOB = gql`
    query($jID: String) {
        applicationsForOneJob(jID: $jID) {
            student {
                name
            }
            status
        }
    }
`;

const QUERY_APPLICATION_BY_ID = gql`
    query($jID: String) {
        job(jID: $jID) {
            title
            location
            salary
            jobDescription
            applicationDeadline
            category
        }
    }
`;

export {
    QUERY_APPLICATIONS_FOR_ONE_JOB,
    QUERY_COMPANY_BASIC,
    QUERY_JOBS_COMPANY,
    QUERY_JOB_BY_ID,
    QUERY_APPLICATION_FOR_ONE_JOB,
    QUERY_APPLICATION_BY_ID,
};
