import { gql } from 'apollo-boost';
// var { GraphQLYYYYMMDD } = require('graphql-moment');

const ADD_COMPANY_BASIC = gql`
    mutation(
        $name: String
        $location: String
        $description: String
        $email: String
        $phone: String
    ) {
        addCompanyBasic(
            name: $name
            location: $location
            description: $description
            email: $email
            phone: $phone
        ) {
            error
            message
        }
    }
`;

const UPDATE_COMPANY_BASIC = gql`
    mutation(
        $name: String
        $location: String
        $description: String
        $email: String
        $phone: String
    ) {
        updateCompanyBasic(
            name: $name
            location: $location
            description: $description
            email: $email
            phone: $phone
        ) {
            error
            message
        }
    }
`;

const ADD_JOB = gql`
    mutation(
        $title: String
        $location: String
        $salary: Int
        $jobDescription: String
        $applicationDeadline: String
        $category: String
    ) {
        addJob(
            title: $title
            location: $location
            salary: $salary
            jobDescription: $jobDescription
            applicationDeadline: $applicationDeadline
            category: $category
        ) {
            error
            message
        }
    }
`;

const UPDATE_JOB = gql`
    mutation(
        $jID: String
        $title: String
        $location: String
        $salary: Int
        $jobDescription: String
        $applicationDeadline: String
        $category: String
    ) {
        updateJob(
            jID: $jID
            title: $title
            location: $location
            salary: $salary
            jobDescription: $jobDescription
            applicationDeadline: $applicationDeadline
            category: $category
        ) {
            error
            message
        }
    }
`;

const UPDATE_APPLICATION_STATUS = gql`
    mutation($aID: String, $status: String) {
        changeApplicationStatus(aID: $aID, status: $status) {
            error
            message
        }
    }
`;

export { ADD_COMPANY_BASIC, UPDATE_COMPANY_BASIC, ADD_JOB, UPDATE_JOB, UPDATE_APPLICATION_STATUS };
