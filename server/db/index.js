const mongoose = require('mongoose');

mongoose.connect("mongodb://127.0.0.1:27017/lab3",{
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false
})
.then((result) => console.log("Mongo connection success!"))
.catch((error) => console.log("Mongo connection Fail!"))