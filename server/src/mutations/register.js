const User = require('../../models/user');
const bcrypt = require('bcrypt');

const registration = async (args) => {
    let response = {};
    const { username, email, password, role } = args;
    const digestedPassword = bcrypt.hashSync(password, 10);

    try {
        const user = await User.findOne({ email });

        if (user) {
            response.error = true;
            response.message = 'email has been taken';
            return response;
        }

        let newUser = new User({
            username,
            email,
            password: digestedPassword,
            role,
        });

        await newUser.save();
        response.status = 201;
        response.message = 'user register success';
        return response;
    } catch (e) {
        throw e;
    }
};

module.exports = { registration };
