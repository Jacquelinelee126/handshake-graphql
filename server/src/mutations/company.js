const CompanyProfile = require('../../models/companyProfile');
const Job = require('../../models/job');
const JobApplication = require('../../models/jobApplication');

const mongoose = require('mongoose');

const addCompanyBasic = async (args, { user }) => {
    const { _id } = user;

    let response = {};
    try {
        companyProfile = new CompanyProfile({
            ...args,
            owner: _id,
        });

        await companyProfile.save();

        response.error = false;
        response.message = 'Profile added succesfully';
        return response;
    } catch (err) {
        response.error = true;
        console.log('err in adding ', err);
        response.message = err;
    }
};

const updateCompanyBasic = async (args, { user }) => {
    const userID = user._id;

    try {
        let result = await CompanyProfile.findOne({ owner: mongoose.Types.ObjectId(userID) });

        if (!result) {
            throw new Error("Can't update if the profile doens't exist.");
        } else {
            const _id = result._id;

            await CompanyProfile.findByIdAndDelete(_id);
            const profile = new CompanyProfile({
                owner: mongoose.Types.ObjectId(userID),
                _id,
                ...args,
            });
            // console.log("PROFILE ===>",profile)

            await profile.save();

            return profile;
        }
    } catch (err) {
        throw err;
    }
};

const addJob = async (args, { user }) => {
    const { _id } = user;

    let response = {};
    try {
        const job = new Job({
            owner: _id,
            ...args,
        });
        await job.save();

        response.error = false;
        response.message = 'Job added successfully';
        return response;
    } catch (err) {
        response.error = true;
        response.message = err;
        return response;
    }
};

const updateJob = async (args, { user }) => {
    const userID = user._id;
    const { jID, ...body } = args;

    console.log('args  ind function', args);
    let response = {};
    try {
        let result = await Job.findById(jID);

        if (!result) {
            throw new Error("Can't update experience information if it doens't exist");
        } else {
            console.log('RESULT before UPDATE =======> ', result);
            temp = { ...result._doc };
            console.log('temp =======> ', temp);
            temp = { ...body };
            console.log('temp after body=======> ', temp);
            const job = new Job({
                owner: mongoose.Types.ObjectId(userID),
                _id: result._id,
                ...temp,
            });

            await Job.deleteOne(result);
            await job.save();

            (response.error = false), (response.message = 'Updated successfully!');

            return response;
        }
    } catch (err) {
        (response.error = true), (response.message = err);

        return response;
    }
};

const changeApplicationStatus = async (args, { user }) => {
    const { aID, status } = args;
    console.log('aID', aID);
    let response = {};
    try {
        // console.log('status ====>', status);
        app = await JobApplication.findById(aID);
        // console.log("app===>", app);
        app.status = status;

        await app.save();

        response.error = false;
        response.message = 'Update status successfully!';
        return response;
    } catch (err) {
        console.log(err);
        response.error = true;
        response.message = err;
        return response;
    }
};
module.exports = {
    addCompanyBasic,
    addJob,
    updateCompanyBasic,
    updateJob,
    changeApplicationStatus,
};
