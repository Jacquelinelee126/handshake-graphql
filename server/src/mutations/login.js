const User = require('../../models/user');

const login = async (args) => {
    const { email, password } = args;

    let response = {};

    try {
        const user = await User.findByCredentials(email, password);
        const token = await user.generateAuthToken();

        response.message = token;
        return response;
    } catch (e) {
        throw e;
    }
};

module.exports = { login };
