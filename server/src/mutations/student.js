const Basic = require('../../models/basic');
const Education = require('../../models/education');
const Experience = require('../../models/experience');
const JobApplication = require('../../models/jobApplication');
const User = require('../../models/user');

const mongoose = require('mongoose');

const addBasic = async (args, { user }) => {
    const { _id } = user;
    const owner = mongoose.Types.ObjectId(_id);

    let response = {};
    const basic = new Basic({
        owner,
        ...args,
    });

    try {
        // const basic = await Basic.findOne({ owner: mongoose.Types.ObjectId(userID) });
        // console.log('profile found ? ===> ', basic);
        await basic.save();
        // response.message = "basic creat sucess";

        response.error = false;
        response.message = 'Basic informatin add sucess';
        return response;
    } catch (err) {
        response.error = true;
        response.message = err;
        return response;
    }
};

const addEducation = async (args, { user }) => {
    const { _id } = user;
    const owner = mongoose.Types.ObjectId(_id);
    let response = {};

    const education = new Education({
        owner,
        ...args,
    });

    try {
        await education.save();

        response.error = false;
        response.message = 'Education informatin add sucess';
        return response;
    } catch (err) {
        response.error = true;
        response.message = err;
        return response;
    }
};

const addExperience = async (args, { user }) => {
    const { _id } = user;
    const owner = mongoose.Types.ObjectId(_id);
    let response = {};

    const experience = new Experience({
        owner,
        ...args,
    });

    try {
        await experience.save();

        response.error = false;
        response.message = 'Experience informatin add sucess';
        return response;
    } catch (err) {
        response.error = true;
        response.message = err;
        return response;
    }
};

const updateBasic = async (args, { user }) => {
    const { _id } = user;
    let response = {};

    try {
        let result = await Basic.findOne({ owner: _id });

        if (!result) {
            throw new Error("Can't update if basic information doesn't exist");
        } else {
            // console.log('RESULT before UPDATE =======> ', result);
            temp = { ...result._doc };
            // console.log('temp =======> ', temp);
            temp = { ...args };
            // console.log('temp after body=======> ', temp);
            const basic = new Basic({ owner: _id, ...temp });
            await Basic.deleteOne(result);
            await basic.save();

            response.error = false;
            response.message = 'Basic informatin update sucess';
            return response;
        }
    } catch (err) {
        response.error = true;
        response.message = err;
        return response;
    }
};

//TODO:
const updateEducation = async (args, { user }) => {
    const { _id } = user;
    let response = {};

    try {
        let result = await Education.findOne({ owner: _id });

        if (!result) {
            throw new Error("Can't update if education information doesn't exist");
        } else {
            // console.log('RESULT before UPDATE =======> ', result);
            temp = { ...result._doc };
            // console.log('temp =======> ', temp);
            temp = { ...args };
            // console.log('temp after body=======> ', temp);
            const education = new Education({ owner: _id, ...temp });
            await Education.deleteOne(result);
            await education.save();
            response.error = false;
            response.message = 'Education informatin update sucess';
            return response;
        }
    } catch (err) {
        response.error = true;
        response.message = err;
        return response;
    }
};

//TODO:
const updateExperience = async (args, { user }) => {
    const userID = user._id;
    const { expID, ...body } = args;
    let response = {};
    console.log('args  ind function', args);
    try {
        let result = await Experience.findById(expID);

        if (!result) {
            throw new Error("Can't update experience information if it doens't exist");
        } else {
            console.log('RESULT before UPDATE =======> ', result);
            temp = { ...result._doc };
            console.log('temp =======> ', temp);
            temp = { ...body };
            console.log('temp after body=======> ', temp);
            const exp = new Experience({
                owner: mongoose.Types.ObjectId(userID),
                _id: result._id,
                ...temp,
            });

            response.error = false;
            response.message = 'Experience informatin update sucess';
            return response;
        }
    } catch (err) {
        response.error = true;
        response.message = err;
        return response;
    }
};

const applyJob = async (args, { user }) => {
    const { _id } = user;
    const { jID } = args;

    let response = {};
    try {
        const application = await JobApplication.findOne({
            applicant: _id,
            job: jID,
        });

        if (application) {
            (response.error = true), (response.message = 'You have applied already!');
            return response;
        }

        const result = new JobApplication({
            job: jID,
            applicant: _id,
        });

        await result.save();
        response.error = false;
        response.message = 'Applied successfully';
        return response;
    } catch (err) {
        throw err;
    }
};

module.exports = {
    addBasic,
    addEducation,
    addExperience,
    applyJob,
    updateBasic,
    updateEducation,
    updateExperience,
};
