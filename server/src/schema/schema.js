const graphql = require('graphql');
var { GraphQLYYYYMMDD } = require('graphql-moment');

const { AuthenticationError } = require('apollo-server');
const mongoose = require('mongoose');
const Company = require('../../models/companyProfile');
const Job = require('../../models/job');
const Basic = require('../../models/basic');
const Education = require('../../models/education');
const Experience = require('../../models/experience');
const { registration } = require('../mutations/register');
const { login } = require('../mutations/login');
const {
    addBasic,
    addEducation,
    addExperience,
    updateBasic,
    updateEducation,
    updateExperience,
    applyJob,
} = require('../mutations/student');
const {
    addCompanyBasic,
    addJob,
    updateCompanyBasic,
    updateJob,
    changeApplicationStatus,
} = require('../mutations/company');
const {
    getUser,
    studentGetJobApplications,
    getOneStudentProfile,
    getAllMyJobs,
    getApplicationForOneJob,
} = require('../queries/user');
const { searchStudents, searchJobs } = require('../queries/search');

const {
    GraphQLObjectType,
    GraphQLString,
    GraphQLSchema,
    GraphQLID,
    GraphQLInt,
    GraphQLList,
    GraphQLNonNull,
    GraphQLBoolean,
} = graphql;

const ResponseType = new GraphQLObjectType({
    name: 'Response',
    fields: () => ({
        error: { type: GraphQLBoolean },
        message: { type: GraphQLString },
    }),
});

const UserType = new GraphQLObjectType({
    name: 'User',
    fields: () => ({
        _id: { type: GraphQLID },
        username: { type: GraphQLString },
        email: { type: GraphQLString },
        password: { type: GraphQLString },
        role: { type: GraphQLString },
    }),
});

const BasicType = new GraphQLObjectType({
    name: 'Basic',
    fields: () => ({
        name: { type: GraphQLString },
        dob: { type: GraphQLString },
        city: { type: GraphQLString },
        state: { type: GraphQLString },
        country: { type: GraphQLString },
        skillset: { type: GraphQLString },
        careerObjective: { type: GraphQLString },
    }),
});

const EducationType = new GraphQLObjectType({
    name: 'Education',
    fields: () => ({
        collegeName: { type: GraphQLString },
        location: { type: GraphQLString },
        degree: { type: GraphQLString },
        major: { type: GraphQLString },
        yearOfPassing: { type: GraphQLString },
    }),
});

const ExperienceType = new GraphQLObjectType({
    name: 'Experience',
    fields: () => ({
        _id: { type: GraphQLString },
        companyName: { type: GraphQLString },
        title: { type: GraphQLString },
        location: { type: GraphQLString },
        startDate: { type: GraphQLString },
        endDate: { type: GraphQLString },
        description: { type: GraphQLString },
    }),
});

const StudentProfileType = new GraphQLObjectType({
    name: 'StudentProfile',
    fields: () => ({
        _id: {
            type: GraphQLString,
        },
        basic: {
            type: BasicType,
        },
        education: {
            type: EducationType,
        },
        experience: {
            type: new GraphQLList(ExperienceType),
        },
    }),
});

const JobType = new GraphQLObjectType({
    name: 'Job',
    fields: () => ({
        _id: { type: GraphQLString },
        title: { type: GraphQLString },
        location: { type: GraphQLString },
        salary: { type: GraphQLInt },
        jobDescription: { type: GraphQLString },
        applicationDeadline: { type: GraphQLString },
        category: { type: GraphQLString },
    }),
});

const CompanyType = new GraphQLObjectType({
    name: 'Company',
    fields: () => ({
        owner: { type: GraphQLString },
        name: { type: GraphQLString },
        location: { type: GraphQLString },
        description: { type: GraphQLString },
        email: { type: GraphQLString },
        phone: { type: GraphQLString },
    }),
});

const JobProfileType = new GraphQLObjectType({
    name: 'JobProfile',
    fields: () => ({
        _id: {
            type: GraphQLString,
        },
        title: {
            type: GraphQLString,
        },
        location: {
            type: GraphQLString,
        },
        salary: {
            type: GraphQLInt,
        },
        jobDescription: {
            type: GraphQLString,
        },
        applicationDeadline: {
            type: GraphQLString,
        },
        company: {
            type: CompanyType,
        },
    }),
});

const CompanyJobApplicationType = new GraphQLObjectType({
    name: 'CompanyJobApplication',
    fields: () => ({
        student: { type: BasicType },
        status: { type: GraphQLString },
        createdAt: { type: GraphQLString },
        _id: { type: GraphQLString },
    }),
});

const StudentJobApplicationType = new GraphQLObjectType({
    name: 'StudentJobApplication',
    fields: () => ({
        job: {
            type: JobProfileType,
        },
        status: {
            type: GraphQLString,
        },
        createdAt: {
            type: GraphQLString,
        },
    }),
});

const RootQuery = new GraphQLObjectType({
    name: 'RootQuery',
    fields: {
        currentUser: {
            type: UserType,
            resolve(parent, args, context) {
                if (context.user) {
                    return context.user;
                }
                throw new Error("User hasn't loged in yet");
            },
        },
        students: {
            type: new GraphQLList(StudentProfileType),
            args: {
                searchBy: { type: GraphQLString },
                keyword: { type: GraphQLString },
                major: { type: GraphQLString },
            },

            async resolve(parent, args, context) {
                return searchStudents(args);
            },
        },

        studentProfile: {
            type: StudentProfileType,
            args: {
                sID: { type: GraphQLString },
            },

            async resolve(parent, args, context) {
                if (context.user) {
                    return getOneStudentProfile(args);
                }

                throw new AuthenticationError('unauthorized');
            },
        },
        jobsStudent: {
            type: new GraphQLList(JobProfileType),
            args: {
                searchBy: { type: GraphQLString },
                keyword: { type: GraphQLString },
                category: { type: GraphQLString },
                location: { type: GraphQLString },
            },
            async resolve(parent, args, context) {
                console.log(args);
                if (context.user) {
                    return searchJobs(args);
                }
                throw new AuthenticationError('unauthorized');
            },
        },

        companyProfile: {
            type: CompanyType,
            args: {
                company: { type: GraphQLString },
            },
            async resolve(parent, args) {
                try {
                    return await Company.findOne({
                        name: new RegExp(args.company, 'i'),
                    });
                } catch (e) {
                    throw e;
                }
            },
        },

        job: {
            type: JobType,
            args: {
                jID: { type: GraphQLString },
            },
            async resolve(parent, args) {
                try {
                    return await Job.findById(args.jID);
                } catch (e) {
                    throw e;
                }
            },
        },

        applicationsStudent: {
            type: new GraphQLList(StudentJobApplicationType),
            async resolve(parent, args, context) {
                console.log('context in resolver', context);
                if (context.user) {
                    return studentGetJobApplications(context);
                }

                throw new AuthenticationError('unauthorized');
            },
        },

        jobsCompany: {
            type: new GraphQLList(JobType),
            async resolve(parent, args, context) {
                console.log('context in resolver', context);

                if (context.user) {
                    return getAllMyJobs(context);
                }
                throw new AuthenticationError('unauthorized');
            },
        },

        applicationsForOneJob: {
            type: new GraphQLList(CompanyJobApplicationType),
            args: {
                jID: { type: GraphQLString },
            },
            resolve(parent, args, context) {
                console.log('args to find jid', args);
                if (context.user) {
                    return getApplicationForOneJob(args, context);
                }
                throw new AuthenticationError('unauthorized');
            },
        },

        companyBasic: {
            type: CompanyType,
            async resolve(parent, args, context) {
                console.log('context in company get profile', context.user._id);
                if (context.user) {
                    const res = await Company.findOne({
                        owner: mongoose.Types.ObjectId(context.user._id),
                    });
                    console.log('res', res);
                    return res;
                }

                throw new AuthenticationError('unauthorized');
            },
        },

        basic: {
            type: BasicType,
            async resolve(parent, args, context) {
                console.log('pring context in query basic', context);
                if (context.user) {
                    console.log('_id', context.user._id);
                    const res = await Basic.findOne({
                        owner: context.user._id,
                    });
                    // {
                    // owner: mongoose.Types.ObjectId(context.user._id),
                    // });

                    console.log(res);
                    return res;
                }

                throw new AuthenticationError('unauthorized');
            },
        },
        education: {
            type: EducationType,
            async resolve(parent, args, context) {
                if (context.user) {
                    const res = await Education.findOne({
                        owner: mongoose.Types.ObjectId(context.user._id),
                    });
                    return res;
                }

                throw new AuthenticationError('unauthorized');
            },
        },

        experiences: {
            type: new GraphQLList(ExperienceType),
            async resolve(parent, args, context) {
                if (context.user) {
                    const res = await Experience.find({
                        owner: mongoose.Types.ObjectId(context.user._id),
                    });
                    return res;
                }

                throw new AuthenticationError('unauthorized');
            },
        },

        experience: {
            type: ExperienceType,
            args: {
                expID: {
                    type: GraphQLString,
                },
            },
            async resolve(parent, args, context) {
                if (context.user) {
                    const res = await Experience.findById({
                        _id: mongoose.Types.ObjectId(args.expID),
                    });
                    return res;
                }

                throw new AuthenticationError('unauthorized');
            },
        },
    },
});

const RootMutation = new GraphQLObjectType({
    name: 'RootMutation',
    fields: {
        userRegistration: {
            type: ResponseType,
            args: {
                username: { type: GraphQLString },
                email: { type: GraphQLString },
                password: { type: GraphQLString },
                role: { type: GraphQLString },
            },
            async resolve(parent, args) {
                return registration(args); // get a res
            },
        },
        userLogin: {
            type: ResponseType,
            args: {
                email: { type: GraphQLString },
                password: { type: GraphQLString },
            },
            async resolve(parent, args) {
                try {
                    return login(args);
                } catch (e) {
                    console.log('e', e);
                    throw e;
                }
            },
        },

        addBasic: {
            type: ResponseType,
            args: {
                // owner: { type: GraphQLString },
                name: { type: GraphQLString },
                dob: { type: GraphQLString },
                city: { type: GraphQLString },
                state: { type: GraphQLString },
                country: { type: GraphQLString },
                skillset: { type: GraphQLString },
                careerObjective: { type: GraphQLString },
            },
            async resolve(parent, args, context) {
                console.log('context printing in add basic', context);
                if (context.user) {
                    return addBasic(args, context);
                }

                throw new AuthenticationError('unauthorized');
            },
        },

        addEducation: {
            type: ResponseType,
            args: {
                collegeName: { type: GraphQLString },
                location: { type: GraphQLString },
                degree: { type: GraphQLString },
                major: { type: GraphQLString },
                yearOfPassing: { type: GraphQLString },
            },
            async resolve(parent, args, context) {
                if (context.user) {
                    return addEducation(args, context);
                }

                throw new AuthenticationError('unauthorized');
            },
        },

        addExperience: {
            type: ResponseType,
            args: {
                // owner: { type: GraphQLString },
                companyName: { type: GraphQLString },
                title: { type: GraphQLString },
                location: { type: GraphQLString },
                startDate: { type: GraphQLString },
                endDate: { type: GraphQLString },
                description: { type: GraphQLString },
            },
            async resolve(parent, args, context) {
                if (context.user) {
                    return addExperience(args, context);
                }

                throw new AuthenticationError('unauthorized');
            },
        },

        updateBasic: {
            type: ResponseType,
            args: {
                // owner: { type: GraphQLString },
                name: { type: GraphQLString },
                dob: { type: GraphQLString },
                city: { type: GraphQLString },
                state: { type: GraphQLString },
                country: { type: GraphQLString },
                skillset: { type: GraphQLString },
                careerObjective: { type: GraphQLString },
            },
            async resolve(parent, args, context) {
                if (context.user) {
                    return updateBasic(args, context);
                }

                throw new AuthenticationError('unauthorized');
            },
        },
        updateEducation: {
            type: ResponseType,
            args: {
                collegeName: { type: GraphQLString },
                location: { type: GraphQLString },
                degree: { type: GraphQLString },
                major: { type: GraphQLString },
                yearOfPassing: { type: GraphQLString },
            },
            async resolve(parent, args, context) {
                console.log('context printing in update education', context);
                if (context.user) {
                    return updateEducation(args, context);
                }

                throw new AuthenticationError('unauthorized');
            },
        },

        updateExperience: {
            type: ResponseType,
            args: {
                expID: { type: GraphQLString },
                companyName: { type: GraphQLString },
                title: { type: GraphQLString },
                location: { type: GraphQLString },
                startDate: { type: GraphQLString },
                endDate: { type: GraphQLString },
                description: { type: GraphQLString },
            },
            resolve(parent, args, context) {
                console.log('in schema', args);
                if (context.user) {
                    return updateExperience(args, context);
                }

                throw new AuthenticationError('unauthorized');
            },
        },

        addCompanyBasic: {
            type: ResponseType,
            args: {
                name: { type: GraphQLString },
                location: { type: GraphQLString },
                description: { type: GraphQLString },
                email: { type: GraphQLString },
                phone: { type: GraphQLString },
            },
            resolve(parent, args, context) {
                if (context.user) {
                    return addCompanyBasic(args, context);
                }

                throw new AuthenticationError('unauthorized');
            },
        },

        updateCompanyBasic: {
            type: ResponseType,
            args: {
                name: { type: GraphQLString },
                location: { type: GraphQLString },
                description: { type: GraphQLString },
                email: { type: GraphQLString },
                phone: { type: GraphQLString },
            },
            resolve(parent, args, context) {
                if (context.user) {
                    return updateCompanyBasic(args, context);
                }

                throw new AuthenticationError('unauthorized');
            },
        },
        addJob: {
            type: ResponseType,
            args: {
                // owner: { type: GraphQLString },
                title: { type: GraphQLString },
                location: { type: GraphQLString },
                salary: { type: GraphQLInt },
                jobDescription: { type: GraphQLString },
                applicationDeadline: { type: GraphQLString },
                category: { type: GraphQLString },
            },
            resolve(parent, args, context) {
                if (context.user) {
                    return addJob(args, context);
                }

                throw new AuthenticationError('unauthorized');
            },
        },

        updateJob: {
            type: ResponseType,
            args: {
                jID: { type: GraphQLString },
                title: { type: GraphQLString },
                location: { type: GraphQLString },
                salary: { type: GraphQLInt },
                jobDescription: { type: GraphQLString },
                applicationDeadline: { type: GraphQLString },
                category: { type: GraphQLString },
            },
            resolve(parent, args, context) {
                if (context.user) {
                    return updateJob(args, context);
                }

                throw new AuthenticationError('unauthorized');
            },
        },
        applyJob: {
            type: ResponseType,
            args: {
                jID: { type: GraphQLString },
            },
            resolve(parent, args, context) {
                if (context.user) {
                    return applyJob(args, context);
                }

                throw new AuthenticationError('unauthorized');
            },
        },

        changeApplicationStatus: {
            type: ResponseType,
            args: {
                aID: { type: GraphQLString },
                status: { type: GraphQLString },
            },
            resolve(parent, args, context) {
                console.log('resolver', args);
                if (context.user) {
                    return changeApplicationStatus(args, context);
                }

                throw new AuthenticationError('unauthorized');
            },
        },
    },
});

const schema = new GraphQLSchema({
    query: RootQuery,
    mutation: RootMutation,
});

module.exports = schema;
