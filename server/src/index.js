const express = require('express');
const jwt = require('jsonwebtoken');
const { secretKey } = require('../constants/index');
const { ApolloServer } = require('apollo-server-express');
const bodyParser = require('body-parser');
const schema = require('./schema/schema');

// const jwtAuth = require('./middleware/jwt');
const cors = require('cors');
require('../db');

const app = express();
app.use(bodyParser.json());
app.use(cors({ origin: 'http://localhost:3000', credentials: true }));

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');
    res.setHeader('Access-Control-Allow-Credentials', 'true');
    res.setHeader('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT,DELETE');
    res.setHeader(
        'Access-Control-Allow-Headers',
        'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers',
    );
    res.setHeader('Cache-Control', 'no-cache');
    next();
});

const getUser = (token) => {
    try {
        if (token) {
            const res = jwt.verify(token, secretKey);
            return res;
        }
        return null;
    } catch (err) {
        return null;
    }
};

const server = new ApolloServer({
    schema,
    context: ({ req }) => {
        // Note! This example uses the `req` object to access headers,
        // but the arguments received by `context` vary by integration.
        // This means they will vary for Express, Koa, Lambda, etc.!
        //
        // To find out the correct arguments for a specific integration,
        // see the `context` option in the API reference for `apollo-server`:
        // https://www.apollographql.com/docs/apollo-server/api/apollo-server/

        const tokenWithBearer = req.headers.authorization || '';
        const token = tokenWithBearer.split(' ')[1];
        const user = getUser(token);
        // console.log('user', user);
        // add the user to the context
        return { user };
    },
});

server.applyMiddleware({ app });

// const PORT = process.env.PORT | 5000;
app.listen({ port: 5000 }, () => console.log(`🚀 Server ready at http://localhost:5000/graphql`));
