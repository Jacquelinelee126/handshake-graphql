const User = require('../../models/user');
const JobApplication = require('../../models/jobApplication');
const Job = require('../../models/job');
const mongoose = require('mongoose');

const studentGetJobApplications = async ({ user }) => {
    const { _id } = user;
    console.log(_id);
    try {
        // const applications = await JobApplication.find({
        //     applicant: _id
        // });
        const applications = await JobApplication.aggregate([
            { $match: { applicant: mongoose.Types.ObjectId(_id) } },
            {
                $lookup: {
                    from: 'jobs',
                    localField: 'job', //field from the input documents
                    foreignField: '_id', //field from the documents o
                    as: 'job', // output array field
                },
            },
            { $unwind: '$job' },
            // {
            //     $lookup: {
            //         from: 'basics',
            //         localField: 'applicant',
            //         foreignField: '_owner',
            //         as: 'basic',
            //     },
            // },
        ]);

        console.log('applicaitons', applications);

        return applications;
    } catch (err) {
        throw err;
    }
};

getOneStudentProfile = async (args) => {
    const { sID } = args;

    console.log('sid', sID);
    try {
        // const temp = await User.find({_id:sID})
        temp = await User.aggregate([
            {
                $match: {
                    _id: mongoose.Types.ObjectId(sID),
                },
            },

            {
                $lookup: {
                    from: 'basics',
                    localField: '_id', //field from the input documents
                    foreignField: 'owner', //field from the documents o
                    as: 'basic', // output array field
                },
            },
            { $unwind: '$basic' },

            {
                $lookup: {
                    from: 'educations',
                    localField: '_id', //field from the input documents
                    foreignField: 'owner', //field from the documents of the "from" collection
                    as: 'education', // output array field
                },
            },

            { $unwind: '$education' },
            {
                $lookup: {
                    from: 'experiences',
                    localField: '_id', //field from the input documents
                    foreignField: 'owner', //field from the documents of the "from" collection
                    as: 'experience', // output array field
                },
            },
        ]);

        console.log('resultArray.length====>', temp.length);
        let result = {};
        if (temp.length != 0) {
            result = temp[0];
        }
        console.log('result ====> ', result);
        return result;
    } catch (err) {
        throw err;
    }
};

const getAllMyJobs = async ({ user }) => {
    const { _id } = user;
    try {
        const result = await Job.find({
            owner: _id,
        });

        return result;
    } catch (err) {
        throw err;
    }
};

const getApplicationForOneJob = async (args) => {
    const { jID } = args;
    console.log('jID', jID);
    try {
        const applications = await JobApplication.aggregate([
            {
                $match: { job: mongoose.Types.ObjectId(jID) },
            },
            {
                $lookup: {
                    from: 'basics',
                    localField: 'applicant', //field from the input documents
                    foreignField: 'owner', //field from the documents o
                    as: 'student',
                },
            },
            { $unwind: '$student' },
        ]);

        console.log('applications', applications);
        return applications;
    } catch (err) {
        throw err;
    }
};

module.exports = {
    studentGetJobApplications,
    getOneStudentProfile,
    getAllMyJobs,
    getApplicationForOneJob,
};

// const getUserByName = async (args) => {
//     try {
//         // const basic = await Basic.findOne({ owner: mongoose.Types.ObjectId(userID) });
//         // console.log('profile found ? ===> ', basic);
//         const user = await User.findOne({ name: args.name });
//         // response.message = "basic creat sucess";

//         return user;
//     } catch (err) {
//         throw err;
//     }
// };
