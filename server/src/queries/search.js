const User = require('../../models/user');
const Job = require('../../models/job');
const mongoose = require('mongoose');

// for now just take care of job title
const searchJobs = async (args, context) => {
    let { searchBy, keyword, category, location } = args;

    console.log(category);
    category = category ? category : '';
    location = location ? location : '';

    const title = searchBy === 'title' ? keyword : '';
    const companyName = searchBy === 'companyName' ? keyword : '';
    // console.log('searchBy', searchBy);
    // console.log('keyword', keyword);

    try {
        const jobs = await Job.aggregate([
            {
                $match: {
                    title: new RegExp(title, 'i'),
                    category: new RegExp(category, 'i'),
                    location: new RegExp(location, 'i'),
                },
            },
            {
                $lookup: {
                    from: 'companyprofiles',
                    localField: 'owner', //field from the input documents
                    foreignField: 'owner', //field from the documents o
                    as: 'company', // output array field
                },
            },
            { $unwind: '$company' },
            { $match: { 'company.name': { $regex: new RegExp(companyName, 'i') } } },
        ]);

        return jobs;
    } catch (err) {
        throw err;
    }
};

const searchStudents = async (args, context) => {
    let { searchBy, keyword, major } = args;

    major = major ? major : '';
    const name = searchBy === 'name' ? keyword : '';
    const collegeName = searchBy === 'collegeName' ? keyword : '';
    console.log('keyword', keyword);
    try {
        results = await User.aggregate([
            { $match: { role: 'student' } },
            {
                $lookup: {
                    from: 'basics',
                    localField: '_id', //field from the input documents
                    foreignField: 'owner', //field from the documents o
                    as: 'basic', // output array field
                },
            },
            { $unwind: '$basic' },
            { $match: { 'basic.name': { $regex: new RegExp(name, 'i') } } },
            // { $project: {'basic.name': 1}},
            {
                $lookup: {
                    from: 'educations',
                    localField: '_id', //field from the input documents
                    foreignField: 'owner', //field from the documents of the "from" collection
                    as: 'education', // output array field
                },
            },
            { $unwind: '$education' },
            {
                $match: {
                    'education.collegeName': new RegExp(collegeName, 'i'),
                    'education.major': new RegExp(major, 'i'),
                },
            },
            {
                $project: { role: 0, email: 0, password: 0, createdAt: 0, updatedAt: 0, __v: 0 },
            },
        ]);

        console.log('results', results);
        return results;
    } catch (err) {
        throw err;
    }
};

module.exports = { searchStudents, searchJobs };
