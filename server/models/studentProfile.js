const mongoose = require('mongoose');
const basicSchema = require('./basic');
const educationSchema = require('./education');
const experienceSchema = require('./experience');
// const User = require('./user');

const validator = require('validator');

const studentProfileSchema = new mongoose.Schema(
    {
        owner: {
            type: mongoose.Schema.Types.ObjectId,
            required: true,
            unique: true,
            ref: 'User',
        },
        // basic: basicSchema,
        // education: educationSchema,
        // experience: [experienceSchema],
    },
    {
        timestamps: true,
    },
);

const StudentProfile = mongoose.model('StudentProfile', studentProfileSchema);
module.exports = StudentProfile;
